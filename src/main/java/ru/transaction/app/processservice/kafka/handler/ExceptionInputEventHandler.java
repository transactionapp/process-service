package ru.transaction.app.processservice.kafka.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.transaction.app.processservice.model.TransactionPaymentDto;
import ru.transaction.app.processservice.service.ProcessService;

@Component
public class ExceptionInputEventHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProcessService processService;


    @KafkaListener(topics = "transaction_app_payment_exception_topic", groupId = "${spring.kafka.consumer.group-id}")
    public void handleInput(ConsumerRecord<String, String> record) throws JsonProcessingException {
        String key = record.key();
        String value = record.value();
        TransactionPaymentDto dto = objectMapper.readValue(value, TransactionPaymentDto.class);
        processService.runTransactionProcessRetry(dto, Integer.parseInt(key.split("_")[1]));
    }

}
