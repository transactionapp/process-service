package ru.transaction.app.processservice.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import static ru.transaction.app.processservice.kafka.producer.ProducerUtils.TOPIC_KEY_TEMPLATE;

@Component
public class ExceptionTransactionProducer {


    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void send(String id, String json, int attempts) {
        kafkaTemplate.send("transaction_app_payment_exception_topic", String.format(TOPIC_KEY_TEMPLATE, id, attempts), json);
    }


}
