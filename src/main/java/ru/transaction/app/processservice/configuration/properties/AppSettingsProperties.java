package ru.transaction.app.processservice.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "app.settings")
public class AppSettingsProperties {

    private String mode;
    private TransactionSettingsProperties transaction;

    @Getter
    @Setter
    public static class TransactionSettingsProperties {

        private TransactionExecutionSettingsProperties execution;

        @Getter
        @Setter
        public static class TransactionExecutionSettingsProperties {
            private int maxRetry = 10;
        }
    }

}
