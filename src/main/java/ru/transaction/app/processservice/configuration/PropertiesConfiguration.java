package ru.transaction.app.processservice.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import ru.transaction.app.processservice.configuration.properties.AppSettingsProperties;

@Configuration
@EnableConfigurationProperties({AppSettingsProperties.class})
public class PropertiesConfiguration {
}
