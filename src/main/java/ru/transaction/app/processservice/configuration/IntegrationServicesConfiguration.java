package ru.transaction.app.processservice.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.transaction.app.processservice.adapter.accountservice.AccountServiceAdapter;
import ru.transaction.app.processservice.adapter.accountservice.BankIntegrationAdapter;
import ru.transaction.app.processservice.adapter.accountservice.impl.AccountServiceAdapterDumbImpl;
import ru.transaction.app.processservice.adapter.accountservice.impl.BankIntegrationAdapterDumbImpl;

@Configuration
public class IntegrationServicesConfiguration {


    @Bean
    @ConditionalOnProperty(value = "app.settings.mode", havingValue = "test")
    public AccountServiceAdapter accountServiceAdapter() {
        return new AccountServiceAdapterDumbImpl();
    }

    @Bean
    @ConditionalOnProperty(value = "app.settings.mode", havingValue = "test")
    public BankIntegrationAdapter bankIntegrationAdapter() {
        return new BankIntegrationAdapterDumbImpl();
    }


}
