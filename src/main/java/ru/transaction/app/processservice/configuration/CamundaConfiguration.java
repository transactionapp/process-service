package ru.transaction.app.processservice.configuration;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.ProcessEngineImpl;
import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.cfg.CompositeProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.persistence.StrongUuidGenerator;
import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.components.jobexecutor.SpringJobExecutor;
import org.camunda.bpm.spring.boot.starter.CamundaBpmConfiguration;
import org.camunda.bpm.spring.boot.starter.event.PublishDelegateParseListener;
import org.camunda.bpm.spring.boot.starter.property.CamundaBpmProperties;
import org.camunda.spin.plugin.impl.SpinProcessEnginePlugin;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Primary
@Configuration("processServiceConfiguration")
@EnableConfigurationProperties({CamundaBpmProperties.class})
public class CamundaConfiguration extends CamundaBpmConfiguration {


    @Bean(value = "processEngine")
    public ProcessEngineFactoryBean processEngineFactory(ProcessEngineConfigurationImpl configuration) {
        ProcessEngineFactoryBean processEngineFactoryBean = new ProcessEngineFactoryBean();
        processEngineFactoryBean.setProcessEngineConfiguration(configuration);
        return processEngineFactoryBean;
    }

    @Primary
    @Bean("processEngineConfiguration")
    public ProcessEngineConfigurationImpl processEngineConfiguration(CamundaBpmProperties bpmProperties,
                                                                     DataSource dataSource,
                                                                     ApplicationEventPublisher publisher,
                                                                     ApplicationContext context,
                                                                     PlatformTransactionManager transactionManager) throws IOException {
        SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
        configuration.setProcessEngineName("ProcessEngine");
        configuration.setDataSource(dataSource);
        configuration.setAdminUsers(Collections.singletonList(bpmProperties.getAdminUser().getId()));
        configuration.setTransactionManager(transactionManager);
        configuration.setDatabaseSchemaUpdate(bpmProperties.getDatabase().getSchemaUpdate());
        configuration.setJobExecutorActivate(true);
        configuration.setDbMetricsReporterActivate(false);
        configuration.setMetricsEnabled(false);
        configuration.setInitializeTelemetry(false);
        //  configuration.setHistoryEventHandler(new CustomHistoryEventHandler(this.camundaHistoryProperties));
        configuration.setIdGenerator(new StrongUuidGenerator());
        configuration.setHistory(SpringProcessEngineConfiguration.HISTORY_AUDIT);
        configuration.setDefaultNumberOfRetries(1);
        configuration.setApplicationContext(context);

        String[] resourcesPathsPatterns = bpmProperties.getDeploymentResourcePattern();

        List<Resource> resources = new ArrayList<>();
        for (String resourcesPathsPattern : resourcesPathsPatterns) {
            resources.addAll(List.of(new PathMatchingResourcePatternResolver().getResources(resourcesPathsPattern)));
        }

        configuration.setDeploymentResources(resources.toArray(Resource[]::new));
        configuration.getProcessEnginePlugins().add(new SpinProcessEnginePlugin());
        List<BpmnParseListener> bpmnParseListeners = new ArrayList<>();
        bpmnParseListeners.add(new PublishDelegateParseListener(publisher, bpmProperties.getEventing()));
        configuration.setCustomPostBPMNParseListeners(bpmnParseListeners);
        configuration.getProcessEnginePlugins().add(new CompositeProcessEnginePlugin(createAdminUserConfiguration()));
        return configuration;
    }


    @Bean(name = "jobExecutor")
    public SpringJobExecutor jobExecutor(ProcessEngine processEngine) {
        SpringJobExecutor jobExecutor = new SpringJobExecutor();
        jobExecutor.registerProcessEngine((ProcessEngineImpl) processEngine);
        return jobExecutor;
    }
}
