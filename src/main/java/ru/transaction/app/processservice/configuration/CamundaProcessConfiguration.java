package ru.transaction.app.processservice.configuration;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@DependsOn({"processServiceConfiguration"})
@Configuration
@EnableProcessApplication
public class CamundaProcessConfiguration {


}
