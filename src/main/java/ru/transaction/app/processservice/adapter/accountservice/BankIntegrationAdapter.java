package ru.transaction.app.processservice.adapter.accountservice;

import java.math.BigDecimal;

public interface BankIntegrationAdapter {


    void proceedPaymentByUserId(String userId, BigDecimal amount);

}
