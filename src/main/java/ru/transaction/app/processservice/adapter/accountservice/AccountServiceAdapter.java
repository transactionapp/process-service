package ru.transaction.app.processservice.adapter.accountservice;

import java.math.BigDecimal;


public interface AccountServiceAdapter {

    BigDecimal getUserAccountStatusByUserId(String userId);

    void proceedPayment(String userId, BigDecimal amount);
}
