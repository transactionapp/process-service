package ru.transaction.app.processservice.adapter.camuda.listener;


import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.variable.value.IntegerValue;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.transaction.app.processservice.adapter.camuda.BpmnConstants;
import ru.transaction.app.processservice.configuration.properties.AppSettingsProperties;
import ru.transaction.app.processservice.kafka.producer.DeadLetterHistoryTransactionProducer;
import ru.transaction.app.processservice.kafka.producer.ExceptionTransactionProducer;

@Slf4j
@Component
public class CamundaTransactionErrorEventListener implements ExecutionListener {

    @Autowired
    private ExceptionTransactionProducer exceptionTransactionProducer;

    @Autowired
    private DeadLetterHistoryTransactionProducer deadLetterHistoryTransactionProducer;


    @Autowired
    private AppSettingsProperties appSettingsProperties;

    @Override
    public void notify(DelegateExecution execution) {
        StringValue body = execution.getVariableTyped(BpmnConstants.TRANSACTION_BODY);
        IntegerValue integerValue = execution.getVariableTyped(BpmnConstants.TRANSACTION_ATTEMPTS);
        int attempts = integerValue != null && integerValue.getValue() != null ? integerValue.getValue() : 0;
        int maxAttempts = appSettingsProperties.getTransaction().getExecution().getMaxRetry();
        StringValue id = execution.getVariableTyped(BpmnConstants.TRANSACTION_ID);
        if (attempts >= maxAttempts) {
            deadLetterHistoryTransactionProducer.send(id.getValue(), body.getValue(), attempts);
            return;
        }
        exceptionTransactionProducer.send(id.getValue(), body.getValue(), ++attempts);
    }
}
