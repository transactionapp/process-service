package ru.transaction.app.processservice.adapter.camuda.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.transaction.app.processservice.adapter.accountservice.AccountServiceAdapter;
import ru.transaction.app.processservice.adapter.camuda.BpmnConstants;

import java.math.BigDecimal;

@Slf4j
@Component
public class ProceedPaymentDelegate implements JavaDelegate {

    @Autowired
    private AccountServiceAdapter accountServiceAdapter;

    @Override
    public void execute(DelegateExecution execution) {

        StringValue userId = execution.getVariableTyped(BpmnConstants.TRANSACTION_USER_ID);
        ObjectValue amount = execution.getVariableTyped(BpmnConstants.TRANSACTION_AMOUNT);

        BigDecimal amountParsed = (BigDecimal) amount.getValue();

        accountServiceAdapter.proceedPayment(userId.getValue(), amountParsed);
        execution.setVariable(BpmnConstants.TRANSACTION_SUCCESS, true);
    }
}
