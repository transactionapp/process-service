package ru.transaction.app.processservice.adapter.camuda.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.springframework.stereotype.Component;
import ru.transaction.app.processservice.adapter.camuda.BpmnConstants;

@Slf4j
@Component
public class AcceptPaymentRequestDelegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        StringValue body = delegateExecution.getVariableTyped(BpmnConstants.TRANSACTION_BODY);
        StringValue id = delegateExecution.getVariableTyped(BpmnConstants.TRANSACTION_ID);
        log.info("PAYMENT TRANSACTION WITH ID: {} ACCEPTED", id.getValue());
        log.debug("PAYMENT TRANSACTION WITH BODY: {} ACCEPTED", body.getValue());
    }


}
