package ru.transaction.app.processservice.adapter.camuda;

public class BpmnConstants {


    public static final String TRANSACTION_PROCESS = "Process_transaction_payment";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String TRANSACTION_USER_ID = "transaction_user_id";

    public static final String TRANSACTION_RECIPIENT_ID = "transaction_recipient_id";
    public static final String TRANSACTION_AMOUNT = "transaction_amount";

    public static final String TRANSACTION_BODY = "transaction_body";

    public static final String TRANSACTION_ATTEMPTS = "transaction_attempts";

    public static final String TRANSACTION_INVOICE = "invoice";
    public static final String TRANSACTION_ACCOUNT_MONEY = "account_money";

    public static final String TRANSACTION_SUCCESS = "success";

}
