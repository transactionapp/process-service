package ru.transaction.app.processservice.adapter.camuda.listener;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.variable.value.IntegerValue;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.transaction.app.processservice.adapter.camuda.BpmnConstants;
import ru.transaction.app.processservice.kafka.producer.SuccessHistoryTransactionProducer;

@Component
public class CamundaTransactionSuccessEventListener implements ExecutionListener {

    @Autowired
    private SuccessHistoryTransactionProducer successHistoryTransactionProducer;


    @Override
    public void notify(DelegateExecution execution) {
        StringValue body = execution.getVariableTyped(BpmnConstants.TRANSACTION_BODY);
        IntegerValue integerValue = execution.getVariableTyped(BpmnConstants.TRANSACTION_ATTEMPTS);
        StringValue id = execution.getVariableTyped(BpmnConstants.TRANSACTION_ID);
        int attempts = integerValue != null && integerValue.getValue() != null ? integerValue.getValue() : 0;

        successHistoryTransactionProducer.send(id.getValue(), body.getValue(), attempts);
    }


}
