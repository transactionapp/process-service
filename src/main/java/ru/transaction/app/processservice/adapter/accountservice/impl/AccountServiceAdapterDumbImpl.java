package ru.transaction.app.processservice.adapter.accountservice.impl;

import ru.transaction.app.processservice.adapter.accountservice.AccountServiceAdapter;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;


public class AccountServiceAdapterDumbImpl implements AccountServiceAdapter {

    private final AtomicReference<BigDecimal> AMOUNT = new AtomicReference<>(new BigDecimal("10000.0"));

    @Override
    public BigDecimal getUserAccountStatusByUserId(String userId) {
        return AMOUNT.get();
    }

    @Override
    public void proceedPayment(String userId, BigDecimal amount) {
        AMOUNT.set(AMOUNT.get().subtract(amount));
    }


}
