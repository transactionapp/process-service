package ru.transaction.app.processservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.transaction.app.processservice.model.TransactionPaymentDto;

public interface ProcessService {


    void runTransactionProcess(TransactionPaymentDto transactionPaymentDto) throws JsonProcessingException;

    void runTransactionProcessRetry(TransactionPaymentDto transactionPaymentDto, int attempts) throws JsonProcessingException;

}
