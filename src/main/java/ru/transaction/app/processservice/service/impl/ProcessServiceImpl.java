package ru.transaction.app.processservice.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.transaction.app.processservice.adapter.camuda.BpmnConstants;
import ru.transaction.app.processservice.model.TransactionPaymentDto;
import ru.transaction.app.processservice.service.ProcessService;

@Service
public class ProcessServiceImpl implements ProcessService {


    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RepositoryService repositoryService;

    @Override
    public void runTransactionProcess(TransactionPaymentDto dto) throws JsonProcessingException {
        var variables = Variables.createVariables();
        variables.put(BpmnConstants.TRANSACTION_ID, dto.getId());
        variables.put(BpmnConstants.TRANSACTION_USER_ID, dto.getAuthorId());
        variables.put(BpmnConstants.TRANSACTION_RECIPIENT_ID, dto.getRecipientId());
        variables.put(BpmnConstants.TRANSACTION_AMOUNT, dto.getAmount());
        variables.put(BpmnConstants.TRANSACTION_BODY, objectMapper.writeValueAsString(dto));
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .active()
                .latestVersion()
                .processDefinitionKey(BpmnConstants.TRANSACTION_PROCESS)
                .singleResult();
        runtimeService.startProcessInstanceById(definition.getId(), variables);
    }


    @Override
    public void runTransactionProcessRetry(TransactionPaymentDto dto, int attempts) throws JsonProcessingException {
        var variables = Variables.createVariables();
        variables.put(BpmnConstants.TRANSACTION_ID, dto.getId());
        variables.put(BpmnConstants.TRANSACTION_USER_ID, dto.getAuthorId());
        variables.put(BpmnConstants.TRANSACTION_RECIPIENT_ID, dto.getRecipientId());
        variables.put(BpmnConstants.TRANSACTION_AMOUNT, dto.getAmount());
        variables.put(BpmnConstants.TRANSACTION_BODY, objectMapper.writeValueAsString(dto));
        variables.put(BpmnConstants.TRANSACTION_ATTEMPTS, attempts);
        ProcessDefinition definition = repositoryService
                .createProcessDefinitionQuery()
                .active()
                .latestVersion()
                .processDefinitionKey(BpmnConstants.TRANSACTION_PROCESS)
                .singleResult();
        runtimeService.startProcessInstanceById(definition.getId(), variables);
    }
}
